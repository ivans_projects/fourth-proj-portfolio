var contactbutton = $('.contact');
var newsButton = $('.news');
var teamButton = $('.team');
var galleryButton = $('.gallery');
var menuButton = $('.menu');
var aboutButton = $('.about');
var reservationButton = $('.reservation');
var teamsectionSection = $('#teamsection');
var eeservationSection = $('#reservation');
var tuchwithusSection = $('#tuchwithus');
var newsSection = $("#news");
var gallerySection = $('#gallerySection');
var menuSection = $("#menuSection");
var aboutussection = $(".aboutussection");
var arrowup = $('.arrowup');
var aboutubutton = $('.aboutussection__button');
var aboutussectionhidentext = $('.aboutussection__hidentext');
var closebutton = $('.aboutussection__close');

$(document).ready(function() {
    $('#toggle').click(function() {
        $(this).toggleClass('active');
        $('#overlay').toggleClass('open');
    });
    contactbutton.click(function(){
         $("html, body").animate({
        scrollTop: tuchwithusSection.offset().top 
    }, 500);
         return false;
    });
    newsButton.click(function(){
         $("html, body").animate({
        scrollTop: newsSection.offset().top 
    }, 500);
         return false;
    });
    reservationButton.click(function(){
         $("html, body").animate({
        scrollTop: eeservationSection.offset().top 
    }, 500);
         return false;
    });
    teamButton.click(function(){
         $("html, body").animate({
        scrollTop: teamsectionSection.offset().top 
    }, 500);
         return false;
    });
    galleryButton.click(function(){
         $("html, body").animate({
        scrollTop: gallerySection.offset().top 
    }, 500);
         return false;
    });
    menuButton.click(function(){
         $("html, body").animate({
        scrollTop: menuSection.offset().top 
    }, 500);
         return false;
    });
    aboutButton.click(function(){
         $("html, body").animate({
        scrollTop: aboutussection.offset().top 
    }, 500);
         return false;
    });
    arrowup.on('click', function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
     $(window).scroll(function () {
        if ($(this).scrollTop() > 6000) {
            arrowup.fadeIn();
        } else {
            arrowup.fadeOut();
        }
    });
    aboutubutton.click(function(){
        aboutussectionhidentext.slideDown();
         return false;
    });
    closebutton.click(function(){
        aboutussectionhidentext.slideUp();
    });
});